from mininet.net import mininet
from mininet.cli import cli
from mininet.link import Intf
from mininet.log import setLogLevel, info

def myNet():
	net = Mininet()
	h1 = net.addHost('h1', ip='10.0.0.1/24')
	r1 = net.addHost('r1')
	net.addLink(h1, r1)
	Intf( 'eth1', node=r1 )
	info( '*** Starting Network\n' )
	net.start()
	r1.cmd("ifconfig r1-eth0 0")
	r1.cmd("ifconfig r1-eth1 0")
	r1.cmd("ip addr add 10.0.0.254/24 brd + dev r1-eth0")
	r1.cmd("ip addr add 192.168.56.101/24 brd + dev eth1")
	r1.cmd("echo 1 > /proc/sys/net/ipv4/ip_forwarding")
	h1.cmd("ip route add default via 10.0.0.254")
	CLI(net)
	net.stop()

if __name__ == '__main__':
	setLogLevel( 'info' )
	myNet()